using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnRate = 3f;
    [SerializeField]
    private Transform[] m_SpawnPoints;
    private GameManager m_GameManager;

    // Start is called before the first frame update
    void Start()
    {
        m_GameManager = GameManager.Instance;
        StartCoroutine(SpawnerCoroutine());
        GameManager.Instance.OnScoreIncrease += IncreaseDifficulty;
    }

    IEnumerator SpawnerCoroutine()
    {
        while (true)
        {
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = m_SpawnPoints[Random.Range(0, m_SpawnPoints.Length)].position;
            yield return new WaitForSeconds(m_SpawnRate);
        }
        
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnScoreIncrease -= IncreaseDifficulty;
    }

    private void IncreaseDifficulty(int score)
    {
        if(score > 0 && score % 5 == 0)
        { 
            m_SpawnRate -= 0.5f;
            //Debug.Log("SPAWNER: "+m_SpawnRate);
            if (m_SpawnRate <= 0.5f)
                m_SpawnRate = 0.5f;

        }
    }
}
