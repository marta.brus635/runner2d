using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TierraController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(-m_Speed, 0);
    }

}
