using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntuacioGameOVer : MonoBehaviour
{
    private TMPro.TextMeshProUGUI m_puntuacio;

    void Start()
    {
        m_puntuacio = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        m_puntuacio.text = "Score: " + GameManager.Instance.Score.ToString();
    }
}
