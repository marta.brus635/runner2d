using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PuntuacioGUI : MonoBehaviour
{
    private TMPro.TextMeshProUGUI m_puntuacio;

    private void Start()
    {
        m_puntuacio = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        m_puntuacio.text = GameManager.Instance.Score.ToString();
    }
}
