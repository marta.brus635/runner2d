using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerraSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnRate = 3f;
    [SerializeField]
    private Transform[] m_SpawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = m_SpawnPoints[Random.Range(0, m_SpawnPoints.Length)].position;
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }

}
