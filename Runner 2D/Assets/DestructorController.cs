using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructorController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //print(collision.gameObject);
        if (collision.gameObject.tag == "Enemic" 
            || collision.gameObject.tag == "Tierra" 
            || collision.gameObject.tag == "Background")
            Destroy(collision.gameObject);

    }

}