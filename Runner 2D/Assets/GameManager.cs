using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance
    {
        get { return m_Instance; }
    }

    

    private float m_Time = 0f;
    public float Time
    {
        get { return m_Time; }
        set { m_Time = value; }
    }

    

    public delegate void ScoreIncrease(int score);
    public event ScoreIncrease OnScoreIncrease;

    private int m_Score = 0;
    public int Score
    {
        get { return m_Score; }
        set
        {
            m_Score = value;
            OnScoreIncrease.Invoke(m_Score);
        }
    }

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //Es crida al carregar una escena
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("S'ha carregat una escena: " + scene.name);
        if (scene.name == "SampleScene")
        {
            InitValues();
        }

    }

    private void InitValues()
    {
        m_Score = 0;
        //resetejar la dificultat, etc
    }

    // Update is called once per frame
    void Update()
    {
        //nicament a mode d'exemple
        if (Input.GetKeyDown(KeyCode.L))
            SceneManager.LoadScene("GameOver");

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene("SampleScene");

    }

}
