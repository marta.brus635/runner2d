using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnRate = 3f;

    void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = gameObject.transform.position;
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }
}
