using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 2f;
    [SerializeField]
    private float m_JumpSpeed = 10f;
    private bool m_onAir = false;
    private Rigidbody2D m_rigidBody;
    [SerializeField]
    private Sprite s_Idle;
    [SerializeField]
    private Sprite s_Run;
    [SerializeField]
    private Sprite s_Jump;
    [SerializeField]
    private Sprite s_Dead;
    private SpriteRenderer m_spriteRenderer;
    private bool m_dead = false;

    // Start is called before the first frame update
    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_dead)
            return;
        
        if (Input.GetKey(KeyCode.A))
        {
            m_rigidBody.velocity = new Vector3(-m_Speed, m_rigidBody.velocity.y, 0);
            m_spriteRenderer.sprite = s_Run;
            m_spriteRenderer.flipX = true;
        }

        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            m_rigidBody.velocity = new Vector3(0, m_rigidBody.velocity.y, 0);
            m_spriteRenderer.sprite = s_Idle;
        }

        if (Input.GetKey(KeyCode.D))
        {
            m_rigidBody.velocity = new Vector3(m_Speed, m_rigidBody.velocity.y, 0);
            m_spriteRenderer.sprite = s_Run;
            m_spriteRenderer.flipX = false;
        }

        if (Input.GetKey(KeyCode.Space) && !m_onAir)
        {
            m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x, m_JumpSpeed, 0);
        }

        if (m_onAir)
        {
            m_spriteRenderer.sprite = s_Jump;
        }
        

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        //Debug.Log("COLLISION: La pilota ha entrat a una col�lisi� amb " + col.gameObject);
        if (col.gameObject.tag == "Tierra")
            m_onAir = false;

    }

    void OnCollisionExit2D(Collision2D col)
    {
        //Debug.Log("COLLISION: La pilota ha sortit d'una col�lisi� amb " + col.gameObject);
        if (col.gameObject.tag == "Tierra")
            m_onAir = true;
        
    }

    
    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("TRIGGER: La pilota ha entrat a una col�lisi� amb " + col.gameObject);

        if(col.gameObject.tag == "Moneda")
        {
            GameManager.Instance.Score += 1;
            //print(m_Score);
            Destroy(col.gameObject);
        }


        if (col.gameObject.tag == "Enemic" || col.gameObject.tag == "Destroyer")
        {
            m_spriteRenderer.sprite = s_Dead;
            m_dead = true;
            StartCoroutine(MuerteYDestruccion());
        }
    }

    IEnumerator MuerteYDestruccion()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("GameOver");
        }
    }


}
